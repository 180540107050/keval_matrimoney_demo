package com.example.matrimony_demo.DbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.Bean.Bean_Language;
import com.example.matrimony_demo.Bean.Bean_States;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;

public class Db_Helper extends SQLiteAssetHelper {

    //Database Static Name
    public static final String DATABASE_NAME = "Matrimony.db";

    //Database Table Name
    public static final String tbl_MST_Candidate = "MST_Candidate";
    public static final String tbl_MST_Language = "MST_Language";
    public static final String tbl_MST_State = "MST_States";

    //Column Name Of MST_Candidate
    public static final String col_Candidate_ID = "Candidate_ID";
    public static final String col_Candidate_Name = "Candidate_Name";
    public static final String col_Candidate_Email = "Candidate_Email";
    public static final String col_Candidate_Mobile = "Candidate_Mobile";
    public static final String col_Candidate_City = "Candidate_City";
    public static final String col_Candidate_StateID = "Candidate_StateID";
    public static final String col_Candidate_LanguageID = "Candidate_LanguageID";
    public static final String col_Candidate_Gender = "Candidate_Gender";
    public static final String col_Candidate_DOB = "Candidate_DOB";
    public static final String col_Candidate_Hobbies = "Candidate_Hobbies";
    public static final String col_Candidate_Caste = "Candidate_Caste";
    public static final String col_Candidate_Favorite = "Candidate_Favorite";

    //Column Name Of MST_Language
    public static final String col_Language_ID = "Language_ID";
    public static final String col_Language_Name = "Language_Name";

    //Column Name Of MST_States
    public static final String col_State_ID = "State_ID";
    public static final String col_State_Name = "State_Name";

    //Context
    Context context;

    //Default Constructor
    public Db_Helper(Context context) {
        super(context, DATABASE_NAME, null, 1);
        this.context = context;
    }

    //Get Language Table Array List
    public ArrayList<Bean_Language> all_LanguageList() {

        //Initialize ArrayList
        ArrayList<Bean_Language> languageArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryFullTable(tbl_MST_Language), null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_Language bean_language = new Bean_Language();

                //Set Values Into Objects
                bean_language.setLanguage_ID(result.getInt(0));
                bean_language.setLanguage_Name(result.getString(1));

                //Add Object Into List
                languageArrayList.add(bean_language);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return languageArrayList;
    }

    //Get State Table Array List
    public ArrayList<Bean_States> all_StateList() {

        //Initialize ArrayList
        ArrayList<Bean_States> statesArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryFullTable(tbl_MST_State), null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_States bean_states = new Bean_States();

                //Set Values Into Objects
                bean_states.setState_ID(result.getInt(0));
                bean_states.setState_Name(result.getString(1));

                //Add Object Into List
                statesArrayList.add(bean_states);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return statesArrayList;
    }

    //Return Count Of Any Table
    public int table_count(String table_Name) {
        //SQLite Database Object Initialize
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor cursor = db.rawQuery(selectQueryFullTable(table_Name), null);

        //Counter Value For Table Value Count
        int count = cursor.getCount();

        //Database Object Close
        db.close();

        //Return Count Of Table Value
        return count;
    }

    //Insert Candidate Record Into Database
    public void insert_CandidateRecord(Bean_Candidate bc) {
        //SQLite Database Object Initialize
        SQLiteDatabase db = this.getWritableDatabase();

        //Content Value For Insert Value Into Database Table
        ContentValues cv = new ContentValues();

        //Insert Data Into Content Value
        cv.put(col_Candidate_Name, bc.getCandidate_Name());
        cv.put(col_Candidate_Email, bc.getCandidate_Email());
        cv.put(col_Candidate_Mobile, bc.getCandidate_Mobile());
        cv.put(col_Candidate_City, bc.getCandidate_City());
        cv.put(col_Candidate_StateID, bc.getCandidate_StateID());
        cv.put(col_Candidate_LanguageID, bc.getCandidate_LanguageID());
        cv.put(col_Candidate_Gender, bc.getCandidate_Gender());
        cv.put(col_Candidate_DOB, bc.getCandidate_DOB());
        cv.put(col_Candidate_Hobbies, bc.getCandidate_Hobbies());
        cv.put(col_Candidate_Caste, bc.getCandidate_Caste());
        cv.put(col_Candidate_Favorite, bc.getCandidate_Favorite());

        //Insert ContentValue Into Database
        db.insert(tbl_MST_Candidate, null, cv);

        //Database Object Close
        db.close();

    }

    //Get Candidate Table Array List
    public ArrayList<Bean_Candidate> all_CandidateList() {
        //Initialize ArrayList
        ArrayList<Bean_Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryFullTable(tbl_MST_Candidate), null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_Candidate bean_candidate = new Bean_Candidate();

                //Set Values Into Objects
                bean_candidate.setCandidate_ID(result.getInt(0));
                bean_candidate.setCandidate_Name(result.getString(1));
                bean_candidate.setCandidate_Email(result.getString(2));
                bean_candidate.setCandidate_Mobile(result.getString(3));
                bean_candidate.setCandidate_City(result.getString(4));
                bean_candidate.setCandidate_StateID(result.getInt(5));
                bean_candidate.setCandidate_LanguageID(result.getInt(6));
                bean_candidate.setCandidate_Gender(result.getInt(7));
                bean_candidate.setCandidate_DOB(result.getString(8));
                bean_candidate.setCandidate_Hobbies(result.getString(9));
                bean_candidate.setCandidate_Caste(result.getString(10));
                bean_candidate.setCandidate_Favorite(result.getInt(11));

                //Add Object Into List
                candidateArrayList.add(bean_candidate);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return candidateArrayList;
    }

    //Selected Candidate Details
    public ArrayList<Bean_Candidate> selected_CandidateData(String table_Name, int id) {
        //Initialize ArrayList
        ArrayList<Bean_Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryTableWithId(table_Name, id), null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_Candidate bean_candidate = new Bean_Candidate();

                //Set Values Into Objects
                bean_candidate.setCandidate_ID(result.getInt(0));
                bean_candidate.setCandidate_Name(result.getString(1));
                bean_candidate.setCandidate_Email(result.getString(2));
                bean_candidate.setCandidate_Mobile(result.getString(3));
                bean_candidate.setCandidate_City(result.getString(4));
                bean_candidate.setCandidate_StateID(result.getInt(5));
                bean_candidate.setCandidate_LanguageID(result.getInt(6));
                bean_candidate.setCandidate_Gender(result.getInt(7));
                bean_candidate.setCandidate_DOB(result.getString(8));
                bean_candidate.setCandidate_Hobbies(result.getString(9));
                bean_candidate.setCandidate_Caste(result.getString(10));
                bean_candidate.setCandidate_Favorite(result.getInt(11));

                //Add Object Into List
                candidateArrayList.add(bean_candidate);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return candidateArrayList;
    }

    //Sql Statement For Select Full Table
    public String selectQueryFullTable(String table_Name) {
        //Sql Statement For Select Table
        String sql = "select * from " + table_Name;

        //Return Sql Statement
        return sql;
    }

    //Select Specific Id From Table Sql Statement
    public String selectQueryTableWithId(String table_Name, int id) {

        String where = "";

        if (table_Name == tbl_MST_Candidate) {
            //Sql Statement For Select Table And Select Specific Id
            where = " where Candidate_ID = " + id;
        } else if (table_Name == tbl_MST_Language) {
            //Sql Statement For Select Table And Select Specific Id
            where = " where Language_ID = " + id;
        } else if (table_Name == tbl_MST_State) {
            //Sql Statement For Select Table And Select Specific Id
            where = " where State_ID = " + id;
        } else {

        }

        //Sql Statement For Select Table
        String sql = "select * from " + table_Name + where;

        return sql;
    }

    //Language Id From Get Language Name
    public String getLanguageName(int languageID) {

        //LanguageName Store
        String languageName;

        //Initialize ArrayList
        ArrayList<Bean_Language> languageArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryTableWithId(tbl_MST_Language, languageID), null);

        //Create Model For Use
        Bean_Language bean_language = new Bean_Language();

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            //Set Values Into Objects
            bean_language.setLanguage_ID(result.getInt(0));
            bean_language.setLanguage_Name(result.getString(1));

        }

        languageName = bean_language.getLanguage_Name();

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return languageName;
    }

    //State Id From Get State Name
    public String getStateName(int stateID) {

        //LanguageName Store
        String stateName;

        //Initialize ArrayList
        ArrayList<Bean_States> statesArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(selectQueryTableWithId(tbl_MST_State, stateID), null);

        //Create Model For Use
        Bean_States bean_states = new Bean_States();

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            //Set Values Into Objects
            bean_states.setState_ID(result.getInt(0));
            bean_states.setState_Name(result.getString(1));

        }

        stateName = bean_states.getState_Name();

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return stateName;
    }

    //Selected Search Candidate
    public ArrayList<Bean_Candidate> search_CandidateList(String searchName) {
        //Initialize ArrayList
        ArrayList<Bean_Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Sql Statement
        String where, sqlStatement;
        if (searchName == "" || searchName == null) {
            sqlStatement = selectQueryFullTable(tbl_MST_Candidate);
        } else {
            where = " where Candidate_Name LIKE '%" + searchName + "%' ;";
            sqlStatement = selectQueryFullTable(tbl_MST_Candidate) + where;
        }


        //Cursor For One By One Data Get For List
        Cursor result = db.rawQuery(sqlStatement, null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_Candidate bean_candidate = new Bean_Candidate();

                //Set Values Into Objects
                bean_candidate.setCandidate_ID(result.getInt(0));
                bean_candidate.setCandidate_Name(result.getString(1));
                bean_candidate.setCandidate_Email(result.getString(2));
                bean_candidate.setCandidate_Mobile(result.getString(3));
                bean_candidate.setCandidate_City(result.getString(4));
                bean_candidate.setCandidate_StateID(result.getInt(5));
                bean_candidate.setCandidate_LanguageID(result.getInt(6));
                bean_candidate.setCandidate_Gender(result.getInt(7));
                bean_candidate.setCandidate_DOB(result.getString(8));
                bean_candidate.setCandidate_Hobbies(result.getString(9));
                bean_candidate.setCandidate_Caste(result.getString(10));
                bean_candidate.setCandidate_Favorite(result.getInt(11));

                //Add Object Into List
                candidateArrayList.add(bean_candidate);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return candidateArrayList;
    }

    //Favorite 0 and 1
    public void changeFavorite(int value,int id){
        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getWritableDatabase();

        //Where Statement
        String where = " where "+col_Candidate_ID+" = "+id;

        //Sql Statement
        String sql = "update "+tbl_MST_Candidate+" set "+col_Candidate_Favorite+" = "+value+where;

        //Execute Query
        db.execSQL(sql);
    }

    //Get Candidate Table Favorite Array List
    public ArrayList<Bean_Candidate> all_FavoriteCandidateList() {
        //Initialize ArrayList
        ArrayList<Bean_Candidate> candidateArrayList = new ArrayList<>();

        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getReadableDatabase();

        //Cursor For One By One Data Get For List
        String where = " where "+col_Candidate_Favorite+"= 1 ;";
        Cursor result = db.rawQuery(selectQueryFullTable(tbl_MST_Candidate)+where, null);

        //Start From First Result To End Loop
        if (result.moveToFirst()) {
            do {
                //New Object For Get Data And Add Into Array List
                Bean_Candidate bean_candidate = new Bean_Candidate();

                //Set Values Into Objects
                bean_candidate.setCandidate_ID(result.getInt(0));
                bean_candidate.setCandidate_Name(result.getString(1));
                bean_candidate.setCandidate_Email(result.getString(2));
                bean_candidate.setCandidate_Mobile(result.getString(3));
                bean_candidate.setCandidate_City(result.getString(4));
                bean_candidate.setCandidate_StateID(result.getInt(5));
                bean_candidate.setCandidate_LanguageID(result.getInt(6));
                bean_candidate.setCandidate_Gender(result.getInt(7));
                bean_candidate.setCandidate_DOB(result.getString(8));
                bean_candidate.setCandidate_Hobbies(result.getString(9));
                bean_candidate.setCandidate_Caste(result.getString(10));
                bean_candidate.setCandidate_Favorite(result.getInt(11));

                //Add Object Into List
                candidateArrayList.add(bean_candidate);

            } while (result.moveToNext());
        }

        //Database Object Close
        db.close();
        //Cursor Object Close
        result.close();

        //Return Array List
        return candidateArrayList;
    }


    //Update Candidate Record
    public void update_CandidateRecord(Bean_Candidate bc) {
        //Initialize SQLite Database Object
        SQLiteDatabase db = this.getWritableDatabase();

        //Content Value For Insert Value Into Database Table
        ContentValues cv = new ContentValues();

        //Insert Data Into Content Value
        cv.put(col_Candidate_Name, bc.getCandidate_Name());
        cv.put(col_Candidate_Email, bc.getCandidate_Email());
        cv.put(col_Candidate_Mobile, bc.getCandidate_Mobile());
        cv.put(col_Candidate_City, bc.getCandidate_City());
        cv.put(col_Candidate_StateID, bc.getCandidate_StateID());
        cv.put(col_Candidate_LanguageID, bc.getCandidate_LanguageID());
        cv.put(col_Candidate_Gender, bc.getCandidate_Gender());
        cv.put(col_Candidate_DOB, bc.getCandidate_DOB());
        cv.put(col_Candidate_Hobbies, bc.getCandidate_Hobbies());
        cv.put(col_Candidate_Caste, bc.getCandidate_Caste());
        cv.put(col_Candidate_Favorite, bc.getCandidate_Favorite());

        //Update ContentValue Into Database
        db.update(tbl_MST_Candidate,cv," "+ col_Candidate_ID + " = "+bc.getCandidate_ID() ,null);

        //Database Object Close
        db.close();

    }

    //Delete Candidate Record
    public void deleteCandidateRecord(int id){
        //Get Database Instance For Write Some Data
        SQLiteDatabase db = this.getWritableDatabase();

        //Query For Delete Record
        String query = "Delete from "+tbl_MST_Candidate+" where "+col_Candidate_ID+"  = "+id;

        //Execute Query
        db.execSQL(query);

        //Close Database Object
        db.close();
    }

}
