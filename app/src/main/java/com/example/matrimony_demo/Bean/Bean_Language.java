package com.example.matrimony_demo.Bean;

public class Bean_Language {
    private int Language_ID;
    private String Language_Name;

    public int getLanguage_ID() {
        return Language_ID;
    }

    public void setLanguage_ID(int language_ID) {
        Language_ID = language_ID;
    }

    public String getLanguage_Name() {
        return Language_Name;
    }

    public void setLanguage_Name(String language_Name) {
        Language_Name = language_Name;
    }
}
