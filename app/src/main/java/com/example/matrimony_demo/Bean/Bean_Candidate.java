package com.example.matrimony_demo.Bean;

public class Bean_Candidate {

    private int Candidate_ID;
    private String Candidate_Name;
    private String Candidate_Email;
    private String Candidate_Mobile;
    private String Candidate_City;
    private int Candidate_StateID;
    private int Candidate_LanguageID;
    private int Candidate_Gender;
    private String Candidate_DOB;
    private String Candidate_Hobbies;
    private String Candidate_Caste;
    private int Candidate_Favorite;

    public int getCandidate_ID() {
        return Candidate_ID;
    }

    public void setCandidate_ID(int candidate_ID) {
        Candidate_ID = candidate_ID;
    }

    public String getCandidate_Name() {
        return Candidate_Name;
    }

    public void setCandidate_Name(String candidate_Name) {
        Candidate_Name = candidate_Name;
    }

    public String getCandidate_Email() {
        return Candidate_Email;
    }

    public void setCandidate_Email(String candidate_Email) {
        Candidate_Email = candidate_Email;
    }

    public String getCandidate_Mobile() {
        return Candidate_Mobile;
    }

    public void setCandidate_Mobile(String candidate_Mobile) {
        Candidate_Mobile = candidate_Mobile;
    }

    public String getCandidate_City() {
        return Candidate_City;
    }

    public void setCandidate_City(String candidate_City) {
        Candidate_City = candidate_City;
    }

    public int getCandidate_StateID() {
        return Candidate_StateID;
    }

    public void setCandidate_StateID(int candidate_StateID) {
        Candidate_StateID = candidate_StateID;
    }

    public int getCandidate_LanguageID() {
        return Candidate_LanguageID;
    }

    public void setCandidate_LanguageID(int candidate_LanguageID) {
        Candidate_LanguageID = candidate_LanguageID;
    }

    public int getCandidate_Gender() {
        return Candidate_Gender;
    }

    public void setCandidate_Gender(int candidate_Gender) {
        Candidate_Gender = candidate_Gender;
    }

    public String getCandidate_DOB() {
        return Candidate_DOB;
    }

    public void setCandidate_DOB(String candidate_DOB) {
        Candidate_DOB = candidate_DOB;
    }

    public String getCandidate_Hobbies() {
        return Candidate_Hobbies;
    }

    public void setCandidate_Hobbies(String candidate_Hobbies) {
        Candidate_Hobbies = candidate_Hobbies;
    }

    public String getCandidate_Caste() {
        return Candidate_Caste;
    }

    public void setCandidate_Caste(String candidate_Caste) {
        Candidate_Caste = candidate_Caste;
    }

    public int getCandidate_Favorite() {
        return Candidate_Favorite;
    }

    public void setCandidate_Favorite(int candidate_Favorite) {
        Candidate_Favorite = candidate_Favorite;
    }
}
