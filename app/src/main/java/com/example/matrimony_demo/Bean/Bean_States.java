package com.example.matrimony_demo.Bean;

public class Bean_States {

    private int State_ID;
    private String State_Name;

    public String getState_Name() {
        return State_Name;
    }

    public void setState_Name(String state_Name) {
        State_Name = state_Name;
    }

    public int getState_ID() {
        return State_ID;
    }

    public void setState_ID(int state_ID) {
        State_ID = state_ID;
    }
}
