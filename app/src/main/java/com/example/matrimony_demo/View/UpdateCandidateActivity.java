package com.example.matrimony_demo.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.matrimony_demo.Adapter.Adapter_Sp_Language;
import com.example.matrimony_demo.Adapter.Adapter_Sp_States;
import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.Bean.Bean_Language;
import com.example.matrimony_demo.Bean.Bean_States;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UpdateCandidateActivity extends AppCompatActivity {

    //EditText Variable
    EditText update_candidate_activity_etName, update_candidate_activity_etEmail, update_candidate_activity_etMobile, update_candidate_activity_etCity, update_candidate_activity_etDOB, update_candidate_activity_etCaste, update_candidate_activity_etHobbies;

    //Spinner Variable
    Spinner update_candidate_activity_spState, update_candidate_activity_spLanguage;

    //RadioGroup Variable
    RadioGroup update_candidate_activity_rgGender;

    //RadioButton
    RadioButton update_candidate_activity_rbMale, update_candidate_activity_rbFeMale;

    //Submit Button
    Button update_candidate_activity_btnUpdate;

    //DbHelper Variable
    Db_Helper db_helper;

    //Candidate Id
    int candidateId;

    //String and Int For Assign EditText, Spinner and Radio Button Value
    String _name, _email, _mobile, _city, _dob, _stateName, _languageName, _caste, _hobbies;
    int _gender, _languageId, _stateId;

    //ArrayList Variable
    ArrayList<Bean_Language> languageArrayList;
    ArrayList<Bean_States> statesArrayList;
    ArrayList<Bean_Candidate> candidateArrayList;

    //Calendar Object For DOB
    Calendar myCalendar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_update_candidate_activity);

        init();
        process();
        currentDateDisplayOnDOB();
    }

    private void init() {
        //Database Helper Object Initialize
        db_helper = new Db_Helper(this);

        //Calendar Object Create For Current Time
        myCalendar = Calendar.getInstance();

        //EditText Set That Layout Id
        update_candidate_activity_etName = findViewById(R.id.update_candidate_activity_etName);
        update_candidate_activity_etEmail = findViewById(R.id.update_candidate_activity_etEmail);
        update_candidate_activity_etMobile = findViewById(R.id.update_candidate_activity_etMobile);
        update_candidate_activity_etCity = findViewById(R.id.update_candidate_activity_etCity);
        update_candidate_activity_etDOB = findViewById(R.id.update_candidate_activity_etDOB);
        update_candidate_activity_etCaste = findViewById(R.id.update_candidate_activity_etCaste);
        update_candidate_activity_etHobbies = findViewById(R.id.update_candidate_activity_etHobbies);

        //Spinner Set That Layout Id
        update_candidate_activity_spLanguage = findViewById(R.id.update_candidate_activity_spLanguage);
        update_candidate_activity_spState = findViewById(R.id.update_candidate_activity_spState);

        //Radio Group Set Layout Id
        update_candidate_activity_rgGender = findViewById(R.id.update_candidate_activity_rgGender);

        //Radio Button Set Layout Id
        update_candidate_activity_rbMale = findViewById(R.id.update_candidate_activity_rbMale);
        update_candidate_activity_rbFeMale = findViewById(R.id.update_candidate_activity_rbFeMale);

        //Submit Button
        update_candidate_activity_btnUpdate = findViewById(R.id.update_candidate_activity_btnUpdate);

        //Array List Object Create
        languageArrayList = new ArrayList<>();
        statesArrayList = new ArrayList<>();

        //Database Object Initialize
        db_helper = new Db_Helper(this);

        //Array List
        candidateArrayList = new ArrayList<>();

        //Get From Last Activity
        Intent intent = getIntent();
        candidateId = intent.getIntExtra("Candidate_ID", 0);

        //Get Data From Database
        candidateArrayList = db_helper.selected_CandidateData(Db_Helper.tbl_MST_Candidate, candidateId);

        //All Data Filled In String Variable
        _name = candidateArrayList.get(0).getCandidate_Name();
        _email = candidateArrayList.get(0).getCandidate_Email();
        _mobile = candidateArrayList.get(0).getCandidate_Mobile();
        _city = candidateArrayList.get(0).getCandidate_City();
        _dob = candidateArrayList.get(0).getCandidate_DOB();
        _gender = candidateArrayList.get(0).getCandidate_Gender();
        _caste = candidateArrayList.get(0).getCandidate_Caste();
        _hobbies = candidateArrayList.get(0).getCandidate_Hobbies();
        //Language Name
        _languageName = db_helper.getLanguageName(candidateArrayList.get(0).getCandidate_LanguageID());
        //State Name
        _stateName = db_helper.getStateName(candidateArrayList.get(0).getCandidate_StateID());
    }

    private void process() {
        //Set Value On TextView
        update_candidate_activity_etName.setText(_name);
        update_candidate_activity_etEmail.setText(_email);
        update_candidate_activity_etMobile.setText(_mobile);
        update_candidate_activity_etCity.setText(_city);
        update_candidate_activity_etDOB.setText(_dob);
        update_candidate_activity_etCaste.setText(_caste);
        update_candidate_activity_etHobbies.setText(_hobbies);

        //Set Radio Button
        if (_gender == 1) {
            //Set Checked Male
            update_candidate_activity_rbMale.setChecked(true);
        } else {
            //Set Checked Female
            update_candidate_activity_rbFeMale.setChecked(true);
        }

        try {
            //State Spinner Set
            statesArrayList = db_helper.all_StateList();
            update_candidate_activity_spState.setAdapter(new Adapter_Sp_States(statesArrayList, this));

            //State Spinner Click Listener
            update_candidate_activity_spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _stateId = statesArrayList.get(position).getState_ID();
                    _stateName = statesArrayList.get(position).getState_Name();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //Language Spinner Set
            languageArrayList = db_helper.all_LanguageList();
            update_candidate_activity_spLanguage.setAdapter(new Adapter_Sp_Language(languageArrayList, this));


            //Language Spinner Click Listener
            update_candidate_activity_spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _languageId = languageArrayList.get(position).getLanguage_ID();
                    _languageName = languageArrayList.get(position).getLanguage_Name();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        //On Click EditText DOB Open Dialog And Change Date
        update_candidate_activity_etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(UpdateCandidateActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //When Submit Data
        update_candidate_activity_btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Edit Text Value Get Into Strings
                _name = update_candidate_activity_etName.getText().toString();
                _email = update_candidate_activity_etEmail.getText().toString();
                _mobile = update_candidate_activity_etMobile.getText().toString();
                _city = update_candidate_activity_etCity.getText().toString();
                _dob = update_candidate_activity_etDOB.getText().toString();
                _caste = update_candidate_activity_etCaste.getText().toString();
                _hobbies = update_candidate_activity_etHobbies.getText().toString();

                //Gender Value Get
                if (update_candidate_activity_rbMale.isChecked() == true) {
                    //Male Is 1
                    _gender = 1;
                } else {
                    //Female Is 2
                    _gender = 2;
                }

                if (dataIsValid()) {

                    //Create Model Of Candidate Object
                    final Bean_Candidate bc = new Bean_Candidate();
                    bc.setCandidate_ID(candidateId);
                    bc.setCandidate_Name(_name);
                    bc.setCandidate_Email(_email);
                    bc.setCandidate_Mobile(_mobile);
                    bc.setCandidate_City(_city);
                    bc.setCandidate_DOB(_dob);
                    bc.setCandidate_StateID(_stateId);
                    bc.setCandidate_LanguageID(_languageId);
                    bc.setCandidate_Gender(_gender);
                    bc.setCandidate_Caste(_caste);
                    bc.setCandidate_Hobbies(_hobbies);

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                //Clicked Yes Then
                                case DialogInterface.BUTTON_POSITIVE:

                                    db_helper.update_CandidateRecord(bc);
                                    int count = db_helper.table_count(Db_Helper.tbl_MST_Candidate);
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No Button Clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(UpdateCandidateActivity.this);
                    builder.setMessage("Are you sure update record?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                }
            }
        });


    }

    //Check Data Is Valid Or Not
    private boolean dataIsValid() {

        //Check Name Validation
        String nameValid = dataNotEmptyLessOrGreaterCheck(update_candidate_activity_etName.getText().toString(), 3, 20);
        if (!nameValid.equalsIgnoreCase("Valid")) {
            update_candidate_activity_etName.setError(validationPrint("Name", nameValid));
            update_candidate_activity_etName.requestFocus();
            return false;
        }

        //Email Not Blank and Pattern Check
        if (isValidEmail(update_candidate_activity_etEmail.getText().toString())) {
        } else {
            update_candidate_activity_etEmail.setError("Enter Correct Email Address!");
            update_candidate_activity_etEmail.requestFocus();
            return false;
        }

        //Mobile Is Not Blank
        if (TextUtils.isEmpty(update_candidate_activity_etMobile.getText().toString())) {
            update_candidate_activity_etMobile.setError("Please Enter Mobile!!!");
            update_candidate_activity_etMobile.requestFocus();
            return false;
        }

        //Mobile Is 10 Digit
        String tempMobile = update_candidate_activity_etMobile.getText().toString();
        if ((tempMobile.length()) != 10) {
            update_candidate_activity_etMobile.setError("Only Enter 10 Digit Number!!!");
            update_candidate_activity_etMobile.requestFocus();
            return false;
        }

        //City Is Not Blank
        String cityValid = dataNotEmptyLessOrGreaterCheck(update_candidate_activity_etCity.getText().toString(), 3, 15);
        if (!cityValid.equalsIgnoreCase("Valid")) {
            update_candidate_activity_etCity.setError(validationPrint("City", nameValid));
            update_candidate_activity_etCity.requestFocus();
            return false;
        }

        //Caste Is Not Blank
        String casteValid = dataNotEmptyLessOrGreaterCheck(update_candidate_activity_etCaste.getText().toString(), 3, 15);
        if (!casteValid.equalsIgnoreCase("Valid")) {
            update_candidate_activity_etCaste.setError(validationPrint("Caste", casteValid));
            update_candidate_activity_etCaste.requestFocus();
            return false;
        }

        //Hobbies Is Black Small and Greater Check
        String hobbiesValid = dataNotEmptyLessOrGreaterCheck(update_candidate_activity_etHobbies.getText().toString(), 3, 15);
        if (!hobbiesValid.equalsIgnoreCase("Valid")) {
            update_candidate_activity_etHobbies.setError(validationPrint("Hobbies", hobbiesValid));
            update_candidate_activity_etHobbies.requestFocus();
        }

        return true;
    }

    //String For Validation Print
    public String validationPrint(String _whichField, String _valid) {
        if (_valid.equalsIgnoreCase("Empty")) {
            return "Please Enter " + _whichField;
        } else if (_valid.equalsIgnoreCase("Small")) {
            return _whichField + " Is Not Smaller Than 3 Character!!!";
        } else if (_valid.equalsIgnoreCase("Greater")) {
            return _whichField + " Is Not Greater Than 15 Character!!!";
        } else {
            return "";
        }

    }

    //Check Data Is Empty Greater Than Or Less Than
    private String dataNotEmptyLessOrGreaterCheck(String _data, int _small, int _greater) {
        if (TextUtils.isEmpty(_data)) {
            return "Empty";
        } else if (_data.length() < _small) {
            return "Small";
        } else if (_data.length() > _greater) {
            return "Greater";
        } else {
            return "Valid";
        }
    }

    //Default Current Date Set
    public void currentDateDisplayOnDOB() {
        //First DOB Set Date and Format
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        update_candidate_activity_etDOB.setText(dateFormat.format(new Date()));
    }

    //DatePicker Dialog Set Value In Object
    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            //Latest Selected Dialog Date Set On EditText
            updateEtDOB();
        }

    };


    //DOB EditText Change Latest Dialog Box Value
    public void updateEtDOB() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        update_candidate_activity_etDOB.setText(sdf.format(myCalendar.getTime()));
    }

    //Email Validation Function
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }
}