package com.example.matrimony_demo.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.example.matrimony_demo.Adapter.Adapter_List_Candidate;
import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    //TextView To List Empty That Check
    LinearLayout layout_list_linearLayoutEmpty, row_list_candidate_linearLayout;

    //ArrayList To Get Candidate Details
    ArrayList<Bean_Candidate> candidateArrayList;

    //Database Helper Object
    Db_Helper db_helper;

    //Recycler View Of List
    RecyclerView layout_list_recyclerView;

    //Adapter Of List Candidate
    Adapter_List_Candidate adapter_list_candidate;

    //Length Of Array List
    int lengthCandidateArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_list_activity);

        init();
        process();
    }

    private void process() {
        if (lengthCandidateArrayList <= 0) {
            layout_list_recyclerView.setVisibility(View.GONE);
            layout_list_linearLayoutEmpty.setVisibility(View.VISIBLE);
        } else {
            layout_list_recyclerView.setVisibility(View.VISIBLE);
            layout_list_linearLayoutEmpty.setVisibility(View.GONE);
            recyclerViewSet();
        }
    }

    private void init() {

        //Database Object Initialize
        db_helper = new Db_Helper(this);

        //Recycler View Object Initialize
        layout_list_recyclerView = findViewById(R.id.layout_list_recyclerView);

        //Array Data Get From Database
        candidateArrayList = db_helper.all_CandidateList();

        //Length Of Candidate Array List
        lengthCandidateArrayList = candidateArrayList.size();

        //LinearLayout Empty Initialize
        layout_list_linearLayoutEmpty = findViewById(R.id.layout_list_linearLayoutEmpty);

        //LinearLayout Of Name Field
        row_list_candidate_linearLayout = findViewById(R.id.row_list_candidate_linearLayout);

    }

    public void recyclerViewSet() {
        //Adapter Data Fill Up ArrayList
        adapter_list_candidate = new Adapter_List_Candidate(candidateArrayList, this);

        layout_list_recyclerView.setLayoutManager(new LinearLayoutManager(this));
        layout_list_recyclerView.setAdapter(adapter_list_candidate);
    }
}