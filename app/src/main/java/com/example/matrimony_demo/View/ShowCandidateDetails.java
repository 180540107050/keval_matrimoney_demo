package com.example.matrimony_demo.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;

import java.util.ArrayList;

public class ShowCandidateDetails extends AppCompatActivity {

    //TextView Object
    TextView show_activity_details_tvName,show_activity_details_tvEmail;
    TextView show_activity_details_tvMobile,show_activity_details_tvCity;
    TextView show_activity_details_tvDOB,show_activity_details_tvState;
    TextView show_activity_details_tvLanguage,show_activity_details_tvCaste;
    TextView show_activity_details_tvHobbies;

    //Update Button
    Button show_activity_details_btnUpdate;

    //RadioButton Object
    RadioButton show_activity_details_rbMale,show_activity_details_rbFeMale;

    //Database Object
    Db_Helper db_helper;

    //Array List
    ArrayList<Bean_Candidate> candidateArrayList;

    //All Variable For Display Data
    int candidateId;
    int _gender;
    String _name,_email,_mobile,_city,_dob,_caste,_hobbies,_languageName,_stateName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_show_candidate_details);

        init();
        process();
        listener();
    }

    public void listener() {
        show_activity_details_btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ShowCandidateDetails.this, UpdateCandidateActivity.class);
                i.putExtra("Candidate_ID", candidateId);
                startActivity(i);
            }
        });
    }

    private void process() {
        //Set Value On TextView
        show_activity_details_tvName.setText(_name);
        show_activity_details_tvEmail.setText(_email);
        show_activity_details_tvMobile.setText(_mobile);
        show_activity_details_tvCity.setText(_city);
        show_activity_details_tvDOB.setText(_dob);
        show_activity_details_tvState.setText(_stateName);
        show_activity_details_tvLanguage.setText(_languageName);
        show_activity_details_tvCaste.setText(_caste);
        show_activity_details_tvHobbies.setText(_hobbies);

        //Set Radio Button
        if(_gender==1){
            //Set Checked Male
            show_activity_details_rbMale.setChecked(true);
        }else{
            show_activity_details_rbFeMale.setChecked(true);
        }
    }

    private void init() {

        //All TextView Initialize By Id
        show_activity_details_tvName = findViewById(R.id.show_activity_details_tvName);
        show_activity_details_tvEmail = findViewById(R.id.show_activity_details_tvEmail);
        show_activity_details_tvMobile = findViewById(R.id.show_activity_details_tvMobile);
        show_activity_details_tvCity = findViewById(R.id.show_activity_details_tvCity);
        show_activity_details_tvDOB = findViewById(R.id.show_activity_details_tvDOB);
        show_activity_details_tvState = findViewById(R.id.show_activity_details_tvState);
        show_activity_details_tvLanguage = findViewById(R.id.show_activity_details_tvLanguage);
        show_activity_details_tvCaste = findViewById(R.id.show_activity_details_tvCaste);
        show_activity_details_tvHobbies = findViewById(R.id.show_activity_details_tvHobbies);

        //All RadioButton Initialize By Id
        show_activity_details_rbMale = findViewById(R.id.show_activity_details_rbMale);
        show_activity_details_rbFeMale = findViewById(R.id.show_activity_details_rbFeMale);

        //Update Button Initialize By Id
        show_activity_details_btnUpdate = findViewById(R.id.show_activity_details_btnUpdate);

        //Database Object Initialize
        db_helper = new Db_Helper(this);

        //Array List
        candidateArrayList = new ArrayList<>();

        Intent intent = getIntent();
        candidateId = intent.getIntExtra("Candidate_ID",0);

        //Get Data From Database
        candidateArrayList = db_helper.selected_CandidateData(Db_Helper.tbl_MST_Candidate,candidateId);

        //All Data Filled In String Variable
        _name = candidateArrayList.get(0).getCandidate_Name();
        _email = candidateArrayList.get(0).getCandidate_Email();
        _mobile = candidateArrayList.get(0).getCandidate_Mobile();
        _city = candidateArrayList.get(0).getCandidate_City();
        _dob = candidateArrayList.get(0).getCandidate_DOB();
        _gender = candidateArrayList.get(0).getCandidate_Gender();
        _caste = candidateArrayList.get(0).getCandidate_Caste();
        _hobbies = candidateArrayList.get(0).getCandidate_Hobbies();
        //Language Name
        _languageName = db_helper.getLanguageName(candidateArrayList.get(0).getCandidate_LanguageID());
        //State Name
        _stateName = db_helper.getStateName(candidateArrayList.get(0).getCandidate_StateID());

    }
}