package com.example.matrimony_demo.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.matrimony_demo.Adapter.Adapter_List_Candidate;
import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {

    //ArrayList To Get Candidate Details
    ArrayList<Bean_Candidate> candidateSearchArrayList;

    //Database Helper Object
    Db_Helper db_helper;

    //Recycler View Of List
    RecyclerView layout_search_candidate_activity_searchRecyclerView;

    //Adapter Of List Candidate
    Adapter_List_Candidate adapter_search_candidate;

    //Length Of Array List
    int lengthCandidateArrayList;

    //EditText Object
    EditText layout_search_candidate_activity_searchEtText;

    //Linear layout
    LinearLayout layout_search_linearLayoutEmpty;

    //String For Search Data
    String searchName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_search_activity);

        init();
        process();
        listeners();
    }

    private void init() {
        //EditText Initialize
        layout_search_candidate_activity_searchEtText = findViewById(R.id.layout_search_candidate_activity_searchEtText);

        //Database Object Initialize
        db_helper = new Db_Helper(this);

        //Recycler View Object Initialize
        layout_search_candidate_activity_searchRecyclerView = findViewById(R.id.layout_search_candidate_activity_searchRecyclerView);

        //LinearLayout Empty Initialize
        layout_search_linearLayoutEmpty = findViewById(R.id.layout_search_linearLayoutEmpty);

        //Get Database
        getDataFromDatabase();
    }

    private void process() {
        if(lengthCandidateArrayList<=0){
            layout_search_candidate_activity_searchRecyclerView.setVisibility(View.GONE);
            layout_search_linearLayoutEmpty.setVisibility(View.VISIBLE);
        }else{
            layout_search_candidate_activity_searchRecyclerView.setVisibility(View.VISIBLE);
            layout_search_linearLayoutEmpty.setVisibility(View.GONE);
            recyclerViewSet();
        }
    }

    private void listeners() {
        layout_search_candidate_activity_searchEtText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                searchName = s.toString();
                getDataFromDatabase();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    private void recyclerViewSet(){
        //Adapter Data Fill Up ArrayList
        adapter_search_candidate = new Adapter_List_Candidate(candidateSearchArrayList,this);

        layout_search_candidate_activity_searchRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        layout_search_candidate_activity_searchRecyclerView.setAdapter(adapter_search_candidate);
    }

    private void getDataFromDatabase(){
        //Array Data Get From Database
        candidateSearchArrayList = db_helper.search_CandidateList(searchName);

        //Length Of Candidate Array List
        lengthCandidateArrayList = candidateSearchArrayList.size();

        recyclerViewSet();
    }
}