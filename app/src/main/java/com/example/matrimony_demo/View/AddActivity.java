package com.example.matrimony_demo.View;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.matrimony_demo.Adapter.Adapter_Sp_Language;
import com.example.matrimony_demo.Adapter.Adapter_Sp_States;
import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.Bean.Bean_Language;
import com.example.matrimony_demo.Bean.Bean_States;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class AddActivity extends AppCompatActivity {

    //EditText Variable
    EditText add_activity_etName, add_activity_etEmail, add_activity_etMobile, add_activity_etCity, add_activity_etDOB, add_activity_etCaste, add_activity_etHobbies;

    //Spinner Variable
    Spinner add_activity_spLanguage, add_activity_spState;

    //RadioGroup Variable
    RadioGroup add_activity_rgGender;

    //RadioButton
    RadioButton add_activity_rbMale, add_activity_rbFeMale;

    //Submit Button
    Button add_activity_btnSubmit;

    //DbHelper Variable
    Db_Helper db_helper;

    //String and Int For Assign EditText, Spinner and Radio Button Value
    String _name, _email, _mobile, _city, _dob, _stateName, _languageName, _caste, _hobbies;
    int _gender, _languageId, _stateId;

    //ArrayList Variable
    ArrayList<Bean_Language> languageArrayList;
    ArrayList<Bean_States> statesArrayList;

    //Calendar Object For DOB
    Calendar myCalendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_add_activity);

        init();
        process();
        currentDateDisplayOnDOB();
    }

    //Initialize All Object
    public void init() {

        //Database Helper Object Initialize
        db_helper = new Db_Helper(this);

        //Calendar Object Create For Current Time
        myCalendar = Calendar.getInstance();

        //EditText Set That Layout Id
        add_activity_etName = findViewById(R.id.add_activity_etName);
        add_activity_etEmail = findViewById(R.id.add_activity_etEmail);
        add_activity_etMobile = findViewById(R.id.add_activity_etMobile);
        add_activity_etCity = findViewById(R.id.add_activity_etCity);
        add_activity_etDOB = findViewById(R.id.add_activity_etDOB);
        add_activity_etCaste = findViewById(R.id.add_activity_etCaste);
        add_activity_etHobbies = findViewById(R.id.add_activity_etHobbies);

        //Spinner Set That Layout Id
        add_activity_spLanguage = findViewById(R.id.add_activity_spLanguage);
        add_activity_spState = findViewById(R.id.add_activity_spState);

        //Radio Group Set Layout Id
        add_activity_rgGender = findViewById(R.id.add_activity_rgGender);

        //Radio Button Set Layout Id
        add_activity_rbMale = findViewById(R.id.add_activity_rbMale);
        add_activity_rbFeMale = findViewById(R.id.add_activity_rbFeMale);

        //Submit Button
        add_activity_btnSubmit = findViewById(R.id.add_activity_btnSubmit);

        //Array List Object Create
        languageArrayList = new ArrayList<>();
        statesArrayList = new ArrayList<>();

    }

    //All Process Of Activity
    public void process() {
        try {
            //State Spinner Set
            statesArrayList = db_helper.all_StateList();
            add_activity_spState.setAdapter(new Adapter_Sp_States(statesArrayList, this));

            //Language Spinner Set
            languageArrayList = db_helper.all_LanguageList();
            add_activity_spLanguage.setAdapter(new Adapter_Sp_Language(languageArrayList, this));

            //State Spinner Click Listener
            add_activity_spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _stateId = statesArrayList.get(position).getState_ID();
                    _stateName = statesArrayList.get(position).getState_Name();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


            //Language Spinner Click Listener
            add_activity_spLanguage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _languageId = languageArrayList.get(position).getLanguage_ID();
                    _languageName = languageArrayList.get(position).getLanguage_Name();

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        }

        //On Click EditText DOB Open Dialog And Change Date
        add_activity_etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(AddActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        //When Submit Data
        add_activity_btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Edit Text Value Get Into Strings
                _name = add_activity_etName.getText().toString();
                _email = add_activity_etEmail.getText().toString();
                _mobile = add_activity_etMobile.getText().toString();
                _city = add_activity_etCity.getText().toString();
                _dob = add_activity_etDOB.getText().toString();
                _caste = add_activity_etCaste.getText().toString();
                _hobbies = add_activity_etHobbies.getText().toString();

                //Gender Value Get
                if (add_activity_rbMale.isChecked() == true) {
                    //Male Is 1
                    _gender = 1;
                } else {
                    //Female Is 2
                    _gender = 2;
                }

                if (dataIsValid() == true) {

                    //Create Model Of Candidate Object
                    final Bean_Candidate bc = new Bean_Candidate();
                    bc.setCandidate_Name(_name);
                    bc.setCandidate_Email(_email);
                    bc.setCandidate_Mobile(_mobile);
                    bc.setCandidate_City(_city);
                    bc.setCandidate_DOB(_dob);
                    bc.setCandidate_StateID(_stateId);
                    bc.setCandidate_LanguageID(_languageId);
                    bc.setCandidate_Gender(_gender);
                    bc.setCandidate_Caste(_caste);
                    bc.setCandidate_Hobbies(_hobbies);

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                //Clicked Yes Then
                                case DialogInterface.BUTTON_POSITIVE:

                                    db_helper.insert_CandidateRecord(bc);
                                    int count = db_helper.table_count(Db_Helper.tbl_MST_Candidate);
                                    Toast.makeText(AddActivity.this, ""+count, Toast.LENGTH_SHORT).show();
                                    finish();
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No Button Clicked
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(AddActivity.this);
                    builder.setMessage("Are you sure insert record?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                }
            }
        });


    }

    //Check Valid Data Or Not
    public boolean dataIsValid() {
        boolean valid = false;

        //Name Not Blank
        if (TextUtils.isEmpty(add_activity_etName.getText().toString())) {
            add_activity_etName.setError("Please Enter Name!!!");
            add_activity_etName.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Name Of Length Isn't Greater Than 15
        if ((add_activity_etName.getText().toString().length()) > 15) {
            add_activity_etName.setError("Name Is Not Greater Than 15 Character!!!");
            add_activity_etName.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Name Of Length Isn't Less Than 3
        if (((add_activity_etName.getText().toString().length()) < 3)) {
            add_activity_etName.setError("Name Is Not Lower Than 3 Character!!!");
            add_activity_etName.requestFocus();
            return false;
        } else {
            valid = true;
        }


        //Email Not Blank and Pattern Check
        if (isValidEmail(add_activity_etEmail.getText().toString())) {
            valid = true;
        } else {
            add_activity_etEmail.setError("Enter Correct Email Address!");
            add_activity_etEmail.requestFocus();
            return false;
        }

        //Mobile Is Not Blank
        if (TextUtils.isEmpty(add_activity_etMobile.getText().toString())) {
            add_activity_etMobile.setError("Please Enter Mobile!!!");
            add_activity_etMobile.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Mobile Is 10 Digit
        String tempMobile = add_activity_etMobile.getText().toString();
        if ((tempMobile.length()) != 10) {
            add_activity_etMobile.setError("Only Enter 10 Digit Number!!!");
            add_activity_etMobile.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //City Is Not Blank
        if (TextUtils.isEmpty(add_activity_etCity.getText().toString())) {
            add_activity_etCity.setError("Please Enter City!!!");
            add_activity_etCity.requestFocus();
            return false;
        } else {
            valid = true;
        }
        //City Of Length Isn't Greater Than 15
        if ((add_activity_etCity.getText().toString().length()) > 15) {
            add_activity_etCity.setError("City Is Not Greater Than 15 Character!!!");
            add_activity_etCity.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //City Of Length Isn't Less Than 3
        if (((add_activity_etCity.getText().toString().length()) < 3)) {
            add_activity_etCity.setError("City Is Not Lower Than 3 Character!!!");
            add_activity_etCity.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Caste Is Not Blank
        if (TextUtils.isEmpty(add_activity_etCaste.getText().toString())) {
            add_activity_etCaste.setError("Please Enter Caste!!!");
            add_activity_etCaste.requestFocus();
            return false;
        } else {
            valid = true;
        }
        //Caste Of Length Isn't Greater Than 15
        if ((add_activity_etCaste.getText().toString().length()) > 15) {
            add_activity_etCaste.setError("Caste Is Not Greater Than 15 Character!!!");
            add_activity_etCaste.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Caste Of Length Isn't Less Than 3
        if (((add_activity_etCaste.getText().toString().length()) < 3)) {
            add_activity_etCaste.setError("Caste Is Not Lower Than 3 Character!!!");
            add_activity_etCaste.requestFocus();
            return false;
        } else {
            valid = true;
        }

        //Hobbies Not Blank
        if (TextUtils.isEmpty(add_activity_etHobbies.getText().toString())) {
            add_activity_etHobbies.setError("Please Enter Hobbies!!!");
            add_activity_etHobbies.requestFocus();
            return false;
        } else {
            valid = true;
        }//Hobbies Of Length Isn't Greater Than 15
        if ((add_activity_etHobbies.getText().toString().length()) > 25) {
            add_activity_etHobbies.setError("Hobbies Is Not Greater Than 25 Character!!!");
            add_activity_etHobbies.requestFocus();
            return false;
        } else {
            valid = true;
        }
        //Hobbies Of Length Isn't Less Than 3
        if (((add_activity_etHobbies.getText().toString().length()) < 3)) {
            add_activity_etHobbies.setError("Hobbies Is Not Lower Than 3 Character!!!");
            add_activity_etHobbies.requestFocus();
            return false;
        } else {
            valid = true;
        }

        return valid;
    }

    //Default Current Date Set
    public void currentDateDisplayOnDOB() {
        //First DOB Set Date and Format
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        add_activity_etDOB.setText(dateFormat.format(new Date()));
    }

    //DatePicker Dialog Set Value In Object
    final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            //Latest Selected Dialog Date Set On EditText
            updateEtDOB();
        }

    };


    //DOB EditText Change Latest Dialog Box Value
    public void updateEtDOB() {
        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        add_activity_etDOB.setText(sdf.format(myCalendar.getTime()));
    }

    //Email Validation Function
    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

}