package com.example.matrimony_demo;

import android.app.Application;

public class AppController extends Application {
    static AppController instance;

    public static AppController getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }
}
