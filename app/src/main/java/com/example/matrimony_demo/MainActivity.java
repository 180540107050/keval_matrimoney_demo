package com.example.matrimony_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import com.example.matrimony_demo.View.AddActivity;
import com.example.matrimony_demo.View.FavoriteActivity;
import com.example.matrimony_demo.View.ListActivity;
import com.example.matrimony_demo.View.SearchActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    //Card View Variable
    CardView dashboard_add_card, dashboard_list_card, dashboard_search_card, dashboard_favorite_card;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        //Init Card View In Variable
        dashboard_add_card = findViewById(R.id.dashboard_add_candidate_card);
        dashboard_list_card = findViewById(R.id.dashboard_list_candidate_card);
        dashboard_search_card = findViewById(R.id.dashboard_search_candidate_card);
        dashboard_favorite_card = findViewById(R.id.dashboard_favorite_candidate_card);

        //Set Card View Click Listener
        dashboard_add_card.setOnClickListener(this);
        dashboard_list_card.setOnClickListener(this);
        dashboard_search_card.setOnClickListener(this);
        dashboard_favorite_card.setOnClickListener(this);

    }

    //Click Event Check Function
    @Override
    public void onClick(View v) {

        //Click Event Of Button With Open New Activity
        switch (v.getId()) {
            case R.id.dashboard_add_candidate_card:
                openNewActivity("add_Candidate");
                break;

            case R.id.dashboard_list_candidate_card:
                openNewActivity("list_Candidate");
                break;

            case R.id.dashboard_search_candidate_card:
                openNewActivity("search_Candidate");
                break;

            case R.id.dashboard_favorite_candidate_card:
                openNewActivity("favorite_Candidate");
                break;

            default:
                break;
        }
    }

    //Open Activity Function
    private void openNewActivity(String activity_name) {
        Intent i;

        //Open New Intent
        if (activity_name.equalsIgnoreCase("add_Candidate")) {
            i = new Intent(getApplicationContext(), AddActivity.class);
            startActivity(i);
        } else if (activity_name.equalsIgnoreCase("list_Candidate")) {
            i = new Intent(getApplicationContext(), ListActivity.class);
            startActivity(i);
        } else if (activity_name.equalsIgnoreCase("search_Candidate")) {
            i = new Intent(getApplicationContext(), SearchActivity.class);
            startActivity(i);
        } else if (activity_name.equalsIgnoreCase("favorite_Candidate")) {
            i = new Intent(getApplicationContext(), FavoriteActivity.class);
            startActivity(i);
        } else {

        }
    }
}