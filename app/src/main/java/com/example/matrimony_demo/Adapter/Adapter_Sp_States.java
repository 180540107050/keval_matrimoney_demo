package com.example.matrimony_demo.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.matrimony_demo.Bean.Bean_States;
import com.example.matrimony_demo.R;

import java.util.ArrayList;

public class Adapter_Sp_States extends BaseAdapter {

    //Language Array List Initialize
    ArrayList<Bean_States> statesArrayList;

    //Context
    Activity context;

    //ViewHolder Object
    ViewHolder holder;

    public Adapter_Sp_States(ArrayList<Bean_States> statesArrayList, Activity context) {
        this.statesArrayList = statesArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return statesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return statesArrayList.get(position).getState_ID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //LayoutInflater Object Initialize
        LayoutInflater layoutInflater = context.getLayoutInflater();

        //Check ConvertView
        if (convertView == null) {

            //Which Layout Use To Set Values
            convertView = layoutInflater.inflate(R.layout.row_states, null);

            //Holder Initialize
            holder = new Adapter_Sp_States.ViewHolder();

            //TextView Initialize
            holder.row_states_StateID_tv = convertView.findViewById(R.id.row_states_StateID_tv);
            holder.row_states_StateName_tv = convertView.findViewById(R.id.row_states_StateName_tv);

            //Set ConvertView Tag
            convertView.setTag(holder);
        } else {
            //If ConvertView Set That Direct Use
            holder = (Adapter_Sp_States.ViewHolder) convertView.getTag();
        }

        //Get LanguageId And LanguageName From ArrayList
        String stateId = String.valueOf(statesArrayList.get(position).getState_ID());
        String stateName = String.valueOf(statesArrayList.get(position).getState_Name());

        //Set LanguageId And LanguageName Variable Into TextView
        holder.row_states_StateID_tv.setText(stateId);
        holder.row_states_StateName_tv.setText(stateName);

        //Return View
        return convertView;
    }

    class ViewHolder {
        TextView row_states_StateID_tv, row_states_StateName_tv;
    }
}
