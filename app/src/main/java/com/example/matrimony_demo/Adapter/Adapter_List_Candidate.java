package com.example.matrimony_demo.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.matrimony_demo.Bean.Bean_Candidate;
import com.example.matrimony_demo.DbHelper.Db_Helper;
import com.example.matrimony_demo.R;
import com.example.matrimony_demo.View.ShowCandidateDetails;

import java.util.ArrayList;

public class Adapter_List_Candidate extends RecyclerView.Adapter<Adapter_List_Candidate.RecyclerViewHolder> {

    //Array List Object
    ArrayList<Bean_Candidate> candidateArrayList;

    //Context
    Context context;

    //Database Object
    Db_Helper db_helper;

    public Adapter_List_Candidate(ArrayList<Bean_Candidate> candidateArrayList, Context context) {
        this.candidateArrayList = candidateArrayList;
        this.context = context;
        db_helper = new Db_Helper(context);
    }

    @NonNull
    @Override
    public Adapter_List_Candidate.RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_list_candidate, viewGroup, false);

        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final Adapter_List_Candidate.RecyclerViewHolder holder, final int position) {
        //Bean_Candidate bc = candidateArrayList.get(position);
        final String candidateID = String.valueOf(candidateArrayList.get(position).getCandidate_ID());
        holder.row_list_candidate_CandidateID.setText(candidateID);
        String candidateName = String.valueOf(candidateArrayList.get(position).getCandidate_Name());
        holder.row_list_candidate_CandidateName.setText(candidateName);
        String candidateDOB = String.valueOf(candidateArrayList.get(position).getCandidate_DOB());
        holder.row_list_candidate_CandidateDOB.setText(candidateDOB);
        String candidateCity = String.valueOf(candidateArrayList.get(position).getCandidate_City());
        holder.row_list_candidate_CandidateCity.setText(candidateCity);
        int candidateFavorite = candidateArrayList.get(position).getCandidate_Favorite();

        if (candidateFavorite == 0) {
            holder.row_list_candidate_ivfavorite.setImageResource(R.drawable.ic_outline_favorite_border_24);
        } else {
            holder.row_list_candidate_ivfavorite.setImageResource(R.drawable.ic_baseline_favorite_24);
        }

        holder.row_list_candidate_linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int _id = candidateArrayList.get(holder.getAdapterPosition()).getCandidate_ID();
                Intent i = new Intent(context, ShowCandidateDetails.class);
                i.putExtra("Candidate_ID", _id);
                context.startActivity(i);
            }
        });

        holder.row_list_candidate_ivfavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int _id = candidateArrayList.get(holder.getAdapterPosition()).getCandidate_ID();
                int _position = holder.getAdapterPosition();

                if (candidateArrayList.get(_position).getCandidate_Favorite() == 0) {
                    holder.row_list_candidate_ivfavorite.setImageResource(R.drawable.ic_baseline_favorite_24);
                    candidateArrayList.get(_position).setCandidate_Favorite(1);
                    db_helper.changeFavorite(1, _id);
                } else {
                    holder.row_list_candidate_ivfavorite.setImageResource(R.drawable.ic_outline_favorite_border_24);
                    candidateArrayList.get(_position).setCandidate_Favorite(0);
                    db_helper.changeFavorite(0, _id);
                }
            }
        });

        holder.row_list_candidate_ivdelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int _id = candidateArrayList.get(holder.getAdapterPosition()).getCandidate_ID();
                int _position = holder.getAdapterPosition();

                candidateArrayList.remove(_position);
                db_helper.deleteCandidateRecord(_id);
                notifyItemRemoved(_position);
            }
        });



    }

    @Override
    public int getItemCount() {
        return candidateArrayList.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {

        //Text View For Display Id and Name
        TextView row_list_candidate_CandidateID, row_list_candidate_CandidateName, row_list_candidate_CandidateDOB, row_list_candidate_CandidateCity;

        //Favorite ImageView
        ImageView row_list_candidate_ivfavorite;

        //Delete ImageView
        ImageView row_list_candidate_ivdelete;

        //LinearLayout
        LinearLayout row_list_candidate_linearLayout;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            row_list_candidate_CandidateID = itemView.findViewById(R.id.row_list_candidate_CandidateID);
            row_list_candidate_CandidateName = itemView.findViewById(R.id.row_list_candidate_CandidateName);
            row_list_candidate_CandidateDOB = itemView.findViewById(R.id.row_list_candidate_CandidateDOB);
            row_list_candidate_CandidateCity = itemView.findViewById(R.id.row_list_candidate_CandidateCity);
            row_list_candidate_ivfavorite = itemView.findViewById(R.id.row_list_candidate_ivfavorite);
            row_list_candidate_linearLayout = itemView.findViewById(R.id.row_list_candidate_linearLayout);
            row_list_candidate_ivdelete = itemView.findViewById(R.id.row_list_candidate_ivdelete);
        }
    }
}
