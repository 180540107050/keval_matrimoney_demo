package com.example.matrimony_demo.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.matrimony_demo.Bean.Bean_Language;
import com.example.matrimony_demo.R;

import java.util.ArrayList;

public class Adapter_Sp_Language extends BaseAdapter {

    //Language Array List Initialize
    ArrayList<Bean_Language> languageArrayList;

    //Context
    Activity context;

    //ViewHolder Object
    ViewHolder holder;

    public Adapter_Sp_Language(ArrayList<Bean_Language> languageArrayList, Activity context) {
        this.languageArrayList = languageArrayList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return this.languageArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return languageArrayList.get(position).getLanguage_ID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //LayoutInflater Object Initialize
        LayoutInflater layoutInflater = context.getLayoutInflater();

        //Check ConvertView
        if (convertView == null) {

            //Which Layout Use To Set Values
            convertView = layoutInflater.inflate(R.layout.row_language, null);

            //Holder Initialize
            holder = new Adapter_Sp_Language.ViewHolder();

            //TextView Initialize
            holder.row_language_LanguageID_tv = convertView.findViewById(R.id.row_language_LanguageID_tv);
            holder.row_language_LanguageName_tv = convertView.findViewById(R.id.row_language_LanguageName_tv);

            //Set ConvertView Tag
            convertView.setTag(holder);
        }else {
            //If ConvertView Set That Direct Use
            holder = (Adapter_Sp_Language.ViewHolder) convertView.getTag();
        }

        //Get LanguageId And LanguageName From ArrayList
        String languageId = String.valueOf(languageArrayList.get(position).getLanguage_ID());
        String languageName = String.valueOf(languageArrayList.get(position).getLanguage_Name());

        //Set LanguageId And LanguageName Variable Into TextView
        holder.row_language_LanguageID_tv.setText(languageId);
        holder.row_language_LanguageName_tv.setText(languageName);

        //Return View
        return convertView;
    }

    //ViewHolder Class For All Object Declare
    public class ViewHolder {
        TextView row_language_LanguageID_tv, row_language_LanguageName_tv;
    }
}
